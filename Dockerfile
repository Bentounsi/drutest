FROM node:14-alpine

ENV PATH="/code/frontend-apps/node_modules/.bin:${PATH}"

RUN apk add --upgrade --no-cache bash && \
    yarn set version berry && \
    yarn --version
